package main

import (
	"bigtableapp/greeting"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	validator "gopkg.in/asaskevich/govalidator.v4"

	"cloud.google.com/go/bigtable"
	"github.com/spf13/viper"
)

type config struct {
	project  string
	instance string
}

type Response struct {
	Status  int
	Message string
}

// User-provided constants.
const (
	tableName        = "Hello-Bigtable-new"
	columnFamilyName = "cf3"
	columnName       = "greetings"
)

// sliceContains reports whether the provided string is present in the given slice of strings.
func sliceContains(list []string, target string) bool {
	for _, s := range list {
		if s == target {
			return true
		}
	}
	return false
}

//CreateTable Used for creation purpose in bigtable
func CreateTable(w http.ResponseWriter, req *http.Request) {

	conf := initConfig()
	ctx := context.Background()
	// Set up admin client, tables, and column families.
	// NewAdminClient uses Application Default Credentials to authenticate.
	adminClient, err := bigtable.NewAdminClient(ctx, conf.project, conf.instance)
	if err != nil {
		log.Print("Could not create admin client: %v", err)
	}

	tables, err := adminClient.Tables(ctx)
	if err != nil {
		log.Print("Could not fetch table list: %v", err)
	}
	var res Response
	if !sliceContains(tables, tableName) {
		log.Printf("Creating table %s", tableName)
		if err := adminClient.CreateTable(ctx, tableName); err != nil {
			log.Print("Could not create table %s: %v", tableName, err)
		}
		res.Status = http.StatusOK
		res.Message = "Table Created Successfully"
	} else {
		res.Status = http.StatusOK
		res.Message = "Table is already exist"
	}
	resJSON, err := json.Marshal(res)
	if err != nil {
		log.Printf("Fails to Marshal JSON Response %v", err)
	}

	//Set Content-Type header so that clients will know how to read response
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	//Write json response back to response
	w.Write(resJSON)

}

func DeleteTable(w http.ResponseWriter, r *http.Request) {
	conf := initConfig()
	ctx := context.Background()
	// Set up admin client, tables, and column families.
	// NewAdminClient uses Application Default Credentials to authenticate.
	adminClient, err := bigtable.NewAdminClient(ctx, conf.project, conf.instance)
	if err != nil {
		log.Print("Could not create admin client: %v", err)
		return
	}

	if err = adminClient.DeleteTable(ctx, tableName); err != nil {
		log.Print("Could not delete table %s: %v", tableName, err)
		return
	}

	if err = adminClient.Close(); err != nil {
		log.Print("Could not close admin client: %v", err)
		return
	}
	var res Response
	res.Status = http.StatusOK
	res.Message = "Deleted Table "

	resJSON, err := json.Marshal(res)
	if err != nil {
		log.Printf("Fails to Marshal JSON Response %v", err)
	}

	//Set Content-Type header so that clients will know how to read response
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	//Write json response back to response

	w.Write(resJSON)

}

func Fetch(w http.ResponseWriter, req *http.Request) {

	conf := initConfig()
	ctx := context.Background()

	// Set up Bigtable data operations client.
	// NewClient uses Application Default Credentials to authenticate.
	client, err := bigtable.NewClient(ctx, conf.project, conf.instance)
	if err != nil {
		log.Print("Could not create data operations client: %v", err)
	}

	tbl := client.Open(tableName)
	log.Printf("Reading all greeting rows:")
	err = tbl.ReadRows(ctx, bigtable.PrefixRange(columnName), func(row bigtable.Row) bool {
		item := row[columnFamilyName][0]
		log.Printf("\t%s = %s\n", item.Row, string(item.Value))
		return true
	}, bigtable.RowFilter(bigtable.ColumnFilter(columnName)))

	if err = client.Close(); err != nil {
		log.Print("Could not close data operations client: %v", err)
	}
}

func CreateRecord(w http.ResponseWriter, req *http.Request) {
	conf := initConfig()

	//parse request
	ctx := context.Background()
	reqBody, err := ioutil.ReadAll(req.Body)
	if err != nil {
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

	var data greeting.Greeting
	_, errV := validator.ValidateStruct(data)
	if errV != nil {
		log.Print("Invalid Request data", err)
	}
	errU := json.Unmarshal(reqBody, &data)
	if errU != nil {
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

	fmt.Printf("%v", data)

	greetings := data.Greeting

	// Set up Bigtable data operations client.
	// NewClient uses Application Default Credentials to authenticate.
	client, err := bigtable.NewClient(ctx, conf.project, conf.instance)
	if err != nil {
		log.Printf("Could not create data operations client: %v", err)
	}

	tbl := client.Open(tableName)
	muts := make([]*bigtable.Mutation, len(greetings))
	rowKeys := make([]string, len(greetings))

	log.Printf("Writing greeting rows to table")
	for i, greeting := range greetings {
		muts[i] = bigtable.NewMutation()
		muts[i].Set(columnFamilyName, columnName, bigtable.Now(), []byte(greeting))

		rowKeys[i] = fmt.Sprintf("%s%d", columnName, i)
	}

	rowErrs, err := tbl.ApplyBulk(ctx, rowKeys, muts)
	if err != nil {
		log.Print("Could not apply bulk row mutation: %v", err)
	}
	if rowErrs != nil {
		for _, rowErr := range rowErrs {
			log.Printf("Error writing row: %v", rowErr)
		}
		log.Print("Could not write some rows")
	}
}

//Read google cloud bigtable configuration
func initConfig() *config {
	viper.SetConfigName("conf")   // name of config file (without extension)
	viper.AddConfigPath("./conf") // call multiple times to add many search paths
	viper.SetConfigType("json")
	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {             // Handle errors reading the config file
		log.Printf("Fatal error config file: %s ", err)
		return nil
	}
	// Handle errors reading the config file
	if err != nil {
		log.Printf("Fatal error config file:", err)
		return nil
	}
	project := viper.GetString("project")
	instance := viper.GetString("instance")
	fmt.Printf(project)
	fmt.Printf(instance)

	conf := new(config)
	conf.project = project
	conf.instance = instance

	return conf
}

func index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hi SampleBigTableApp")
}
func main() {
	http.HandleFunc("/", index)
	http.HandleFunc("/createTable", CreateTable)
	http.HandleFunc("/createRecord", CreateRecord)
	http.HandleFunc("/fetch", Fetch)
	http.HandleFunc("/deleteTable", DeleteTable)
	log.Fatal(http.ListenAndServe(":8085", nil))
}
